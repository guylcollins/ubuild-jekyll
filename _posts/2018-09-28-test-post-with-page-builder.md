---
title: test post with page builder
layout: blocks
date: 2018-09-28 00:00:00 +0000
page_sections:
- template: simple-header
  block: header-3
- template: 1-column-text
  block: one-column-1
  headline: Here is my single column hedline
  content: this is the content that's going inside here
- template: content-feature
  block: feature-1
  media_alignment: Left
  headline: content feature
  content: here's the content
  media:
    image: "/uploads/2018/06/21/drone-photo.jpeg"
    alt_text: alt text here
menu:
  main:
    weight: 2
    title: Page Built

---
